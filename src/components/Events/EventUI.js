import React from "react";
import styled from "styled-components";
import { Googlecalendar } from "@styled-icons/simple-icons";

export const Card = ({ link, children, isCard }) => {
    if (isCard)
        return (
            <StyledCard href={link} rel="noopener noreferrer" target="_blank">
                {children}
            </StyledCard>
        );
};

export const Heading = styled.h1`
    font-size: 3.5rem;
    text-align: center;
    margin: 1em 0;
    @media (max-width: 50em) {
        font-size: 3em;
        margin-bottom: 1em;
    }
`;

export const ItemContainer = styled.div`
    padding: 5em;
    @media (max-width: 50em) {
        padding: 5em 0;
    }
`;

export const ItemWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;

export const StyledCard = styled.a`
    display: block;
    position: relative;
    max-width: 17em;
    border-radius: 5%;
    padding: 2em 1.5em;
    margin: 1em;
    text-decoration: none;
    z-index: 0;
    overflow: hidden;
    box-shadow: 0 0 5px 1px currentColor;
    &:before {
        content: "";
        position: absolute;
        z-index: -1;
        top: -1em;
        right: -1em;
        height: 2em;
        width: 2em;
        border-radius: 2em;
        transform: scale(1);
        transform-origin: 50% 50%;
        transition: transform 0.3s ease-in-out;
    }

    &:hover:before {
        transform: scale(40);
        color: currentColor;
    }

    &:hover p {
        transition: all 0.3s ease-in-out;
        color: currentColor;
    }
    &:hover span {
        transition: all 0.3s ease-in-out;
        color: currentColor;
    }

    &:hover h2 {
        transition: all 0.3s ease-in-out;
        color: currentColor;
    }
    &:hover svg {
        transition: all 0.3s ease-in-out;
        color: currentColor;
    }
`;

export const CardTitle = styled.h2`
    font-size: 2em;
    font-weight: 700;
    margin-bottom: 1em;
`;

export const CardText = styled.p`
    font-size: 1em;
    font-weight: 400;
    line-height: 1.2em;
`;

export const Corner = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    width: 2em;
    height: 2em;
    overflow: hidden;
    top: 0;
    right: 0;
    border-radius: 0 0.2em 0 2em;
    background: currentColor;
`;

export const DateWrapper = styled.div`
    display: inline-block;
`;

export const CardDate = styled.span`
    font-size: 1.2rem;
    font-weight: 600;
    line-height: 1.5em;
    margin-bottom: 1em;
`;

export const CalendarIcon = styled(Googlecalendar)`
    position: relative;
    height: 2em;
    width: 2em;
    margin-right: 1em;
`;
