import React from "react";
import {
    Card,
    CardTitle,
    CardText,
    CardDate,
    Corner,
    CalendarIcon,
    DateWrapper,
} from "./EventUI";

const EventCards = ({ data }) => {
    const formattedDate = new Date(data.date).toDateString();
    return (
        <Card link={data.link} isCard>
            <CardTitle>{data.name}</CardTitle>
            <DateWrapper>
                <CalendarIcon />
                <CardDate>{formattedDate}</CardDate>
            </DateWrapper>
            <CardText>{data.desc}</CardText>
            <Corner />
        </Card>
    );
};

export default EventCards;
