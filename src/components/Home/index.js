import { homeData } from "../../assets/constants";
import React from "react";
import {
    GlobalStyle,
    CodeSnippet,
    LeftDiv,
    RightDiv,
    ParaStyle,
} from "./HomeUI";
import {
    MainDiv,
    MainDivForHead,
    HStyle,
    ImageStyle,
    GlobalStyleForSmallScreen,
} from "./HomeUI";

const Home = () => {
    return (
        <>
            <RightDiv>
                <GlobalStyleForSmallScreen>
                    {homeData.Title}
                </GlobalStyleForSmallScreen>
            </RightDiv>
            <MainDivForHead>
                <ImageStyle src={homeData.logo} />
                <RightDiv>
                    <GlobalStyle>{homeData.Title}</GlobalStyle>
                </RightDiv>
            </MainDivForHead>
            <MainDiv>
                <LeftDiv>
                    <ImageStyle src={homeData.graphImg} />
                </LeftDiv>
                <RightDiv>
                    <HStyle>{homeData.descHead}</HStyle>
                    <ParaStyle>{homeData.descBox}</ParaStyle>
                </RightDiv>
            </MainDiv>
            <MainDiv>
                <LeftDiv>
                    <CodeSnippet>
                        <pre>
                            <code>{homeData.codeSnippet}</code>
                        </pre>
                    </CodeSnippet>
                </LeftDiv>
                <RightDiv>
                    <ParaStyle>{homeData.Links}</ParaStyle>
                </RightDiv>
            </MainDiv>
        </>
    );
};
export default Home;
