import styled from "styled-components";
export const MainDivForHead = styled.div`
    display: flex;
    width: 75%;
    margin: 0 auto;
    flex-direction: row;
    @media (max-width: 50em) {
        flex-direction: column;
    }
`;
export const MainDiv = styled.div`
    display: flex;
    flex-direction: row;
    width: 75%;
    margin: 2.5em auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    padding: 1em;
    @media (max-width: 50em) {
        flex-direction: column;
        margin-bottom: 1.5em;
    }
`;
export const ImageStyle = styled.img`
    margin: auto;
    width: 15em;
    height: 15em;
    text-align: center;
    @media screen and (max-width: 50em) {
        width: 100%;
        height: auto;
    }
`;
export const GlobalStyle = styled.p`
    font-size: 8rem;
    text-align: center;
    display: block;
    @media screen and (max-width: 50em) {
        display: none;
    }
`;
export const GlobalStyleForSmallScreen = styled.p`
    font-family: -apple-system, BlinkMacSystemFont, Roboto, sans-serif;
    font-size: 10rem;
    font-weight: normal;
    text-align: right;
    display: none;
    @media screen and (max-width: 50em) {
        text-align: center;
        font-size: min(10vw, 5rem);
        display: block;
    }
`;
export const ParaStyle = styled.p`
    font-size: 1.5rem;
    font-family: "Lucida Console", "Courier New", monospace;
    @media screen and (max-width: 50em) {
        font-size: 1rem;
    }
`;
export const HStyle = styled.p`
    font-size: 3rem;
    font-family: "Lucida Console", "Courier New", monospace;
    @media screen and (max-width: 50em) {
        font-size: 2rem;
    }
`;

export const CodeSnippet = styled.div`
    width: 90%;
    padding: 0.5vw 0.5vw 0.5vw 0.5vw;
    font-size: 1rem;
`;
export const LeftDiv = styled.div`
    padding: 0.5vw 0.5vw 0.5vw 0.5vw;
    @media (min-width: 50em) {
        flex: 40%;
    }
`;
export const RightDiv = styled.div`
    padding: 0.5vw 0.5vw 0.5vw 0.5vw;
    @media (min-width: 50em) {
        flex: 60%;
    }
`;
