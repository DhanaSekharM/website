import React from "react";
import Heading from "./Heading";
import Image from "./Image";

const Topology = (props) => {
    return (
        <>
            <Heading>Topology</Heading>
            <Image src={props.topo.img} />
            <p>{props.topo.txt}</p>
        </>
    );
};
export default Topology;
