import styled from "styled-components";

const Heading = styled.h2`
    margin: 2rem 0 0.8rem 0;
`;

export default Heading;
