import styled from "styled-components";
import React, { Component } from "react";
import { Anydesk } from "@styled-icons/simple-icons";
import { reverseTheme } from "../../assets/theme";

export const Content = styled.p`
    padding-left: ${(props) => (props.indent ? "2rem" : "0")};
    span {
        font-weight: 700;
    }
`;

const CopyIcon = styled(Anydesk)`
    height: 1rem;
    width: 1rem;
    margin: 0.5rem;
    cursor: pointer;
    transform: rotate(-45deg);
    &:hover {
        transform: scale(1.5) rotate(-45deg);
    }
`;

const StyledCode = styled.code`
    border: 0.05rem solid gray;
    padding: 0.5rem;
    margin-left: ${(props) => (props.indent ? "2rem" : "0")};
    @media (max-width: 50em) {
        margin-right: 0;
    }
`;

export const Tab = styled.button`
    width: 10rem;
    height: 3rem;
    align-items: center;
    text-align: center;
    border-radius: 1rem;
    border: solid 0.2rem currentColor;
    margin-right: 0.5rem;
    margin-bottom: 2em;
    cursor: pointer;
    ${({ active }) => (active ? reverseTheme : null)}
`;

export const CenterContainer = styled.div`
    display: ${(props) => (props.display ? "flex" : "none")};
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const TabContainer = styled.div`
    display: ${(props) => (props.display ? "flex" : "none")};
    flex-direction: column;
    border: double currentColor 0.2rem;
    padding: 2rem;
    width: 70%;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
`;

export class Code extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <StyledCode indent={this.props.indent}>
                <CopyIcon
                    onClick={() => {
                        navigator.clipboard.writeText(this.props.txt);
                    }}
                />
                {this.props.txt}
            </StyledCode>
        );
    }
}
