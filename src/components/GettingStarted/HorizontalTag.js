import styled from "styled-components";

const HorizontalTag = styled.hr`
    height: 0.05rem;
    border-width: 0;
    margin: 0;
    color: gray;
    background-color: gray;
    width: 80%;
`;
export default HorizontalTag;
