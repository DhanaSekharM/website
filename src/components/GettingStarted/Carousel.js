import React, { useState } from "react";
import Heading from "./Heading";
import Image from "./Image";
import {
    SlideText,
    StyledSlider,
    Arrow,
    StyledPagination,
    Dots,
} from "./Carousel.style";

const zero = 0,
    one = 1;

const Pagination = ({ totalImages, active, clickHandler }) => {
    const dotsArr = [];
    if (totalImages) {
        for (let i = 0; i < totalImages; i++) {
            if (i === active) {
                dotsArr.push(
                    <Dots active key={i} onClick={() => clickHandler(i)}>
                        {i + one}
                    </Dots>
                );
            } else {
                dotsArr.push(
                    <Dots key={i} onClick={() => clickHandler(i)}>
                        {i + one}
                    </Dots>
                );
            }
        }
    }
    return dotsArr;
};

const Carousel = (props) => {
    const [currentImage, setCurrentImage] = useState(zero);
    const totalImages = props.carouselData.length;

    const nextImage = () => {
        setCurrentImage(
            currentImage === totalImages - one ? zero : currentImage + one
        );
    };

    const prevImage = () => {
        setCurrentImage(
            currentImage === zero ? totalImages - one : currentImage - one
        );
    };
    return (
        <>
            <Heading>Graphs and Plots</Heading>
            <StyledSlider>
                {props.carouselData.length &&
                    props.carouselData.map((child, i) => {
                        return (
                            <div key={i}>
                                {i === currentImage && (
                                    <>
                                        <Image
                                            src={child.img}
                                            alt={child.alt}
                                        />
                                        <SlideText>{child.txt}</SlideText>
                                    </>
                                )}
                            </div>
                        );
                    })}
            </StyledSlider>
            <StyledPagination>
                <Arrow left onClick={prevImage} />
                <Pagination
                    totalImages={totalImages}
                    active={currentImage}
                    clickHandler={(val) => setCurrentImage(val)}
                />
                <Arrow onClick={nextImage} />
            </StyledPagination>
        </>
    );
};

export default Carousel;
