import styled from "styled-components";

export const Container = styled.div`
    width: 70vw;
    margin: 0 15vw 0 15vw;
    @media (max-width: 768px) {
        width: 90vw;
        margin: 0 5vw 0 5vw;
    }
`;
export default Container;
