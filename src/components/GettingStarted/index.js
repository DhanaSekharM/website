import React from "react";
import Carousel from "./Carousel";
import Install from "./install";
import { gettingStartedData } from "../../assets/constants/index";
import Intro from "./intro";
import Topology from "./topology";
import Container from "./MainContainer";
const GettingStarted = () => {
    const { carouselData, installationData, introData, topo } =
        gettingStartedData;
    return (
        <Container>
            {introData && (
                <>
                    <Intro introData={introData} />
                    <Topology topo={topo} />
                    <Install installationData={installationData} />
                    <Carousel carouselData={carouselData} />
                </>
            )}
        </Container>
    );
};
export default GettingStarted;
