import React, { useState } from "react";
import {
    Content,
    Code,
    Tab,
    CenterContainer,
    TabContainer,
} from "./install.style";
import Heading from "./Heading";

const Install = (props) => {
    const one = 1,
        two = 2;
    const [active, setActive] = useState(one);

    const handleClick = (e) => {
        const i = parseInt(e.target.id, 0);
        if (i !== active) {
            setActive(i);
        }
    };

    return (
        <>
            <Heading>Installation</Heading>
            <Content>
                <span>Note: </span>
                {props.installationData.note}
            </Content>
            <Heading>Installing NeST python package</Heading>
            <>
                {props.installationData &&
                    props.installationData.general.map((child) => {
                        return (
                            <React.Fragment key={child.txt}>
                                <Content>{child.txt}</Content>
                                <Code txt={child.code} />
                            </React.Fragment>
                        );
                    })}
            </>
            <Content>
                Below are the two approaches to install NeST python package:
            </Content>

            <CenterContainer display>
                <Tab
                    active={active === one}
                    onClick={(e) => handleClick(e)}
                    id={one}
                >
                    1. From PyPi
                </Tab>
                <Tab
                    active={active === two}
                    onClick={(e) => handleClick(e)}
                    id={two}
                >
                    2. From source
                </Tab>
            </CenterContainer>
            <TabContainer display={active === one}>
                <Content>{props.installationData.pyPi.txt}</Content>
                <Code txt={props.installationData.pyPi.code} />
            </TabContainer>
            <TabContainer display={active === two}>
                <Content>{props.installationData.source.txt1}</Content>
                <Content>1. {props.installationData.source.meth1.txt}</Content>
                <Code indent txt={props.installationData.source.meth1.code} />
                <Content>2. {props.installationData.source.txt2}</Content>
                {props.installationData &&
                    props.installationData.source.meth2.map((child) => {
                        return (
                            <React.Fragment key={child.txt}>
                                <Content indent>{child.txt}</Content>
                                <Code indent txt={child.code} />
                            </React.Fragment>
                        );
                    })}
            </TabContainer>
        </>
    );
};
export default Install;
