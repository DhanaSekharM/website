import React, { useState } from "react";
import Card from "./ContributorsComponents/Card";
import styled from "styled-components";

const Container = styled.div`
    padding-inline: 5em;
    overflow-y: scroll;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    height: 30em;
    flex-basis: 100%;
    flex-wrap: wrap;
    gap: 5em;
    @media (max-width: 50em) {
        padding-inline: 2em;
        gap: 2em;
    }
`;

const Contributors = () => {
    const [hubData, setHubData] = useState([]);
    if (!hubData.length) Github();
    async function Github() {
        const url =
            "https://api.github.com/repos/nitk-nest/NeST/contributors?per_page=100";
        let response = await fetch(url);
        let json = await response.json();
        json = Array.from(json);
        setHubData(json);
    }
    // const [labData, setLabData] = useState([]);
    // async function GitLab() {
    //     const url =
    //         "https://gitlab.com/api/v4/projects/15433982/repository/contributors?sort=desc&per_page=100";
    //     const response = await fetch(url);
    //     let data = await response.json();
    //     setLabData(data);
    // }
    // if (!labData.length) GitLab();

    return (
        <Container>
            {/* eslint-disable-next-line camelcase*/}
            {hubData.map(
                ({
                    login,
                    contributions,
                    html_url: htmlUrl,
                    avatar_url: avatarUrl,
                }) => (
                    <Card
                        name={login}
                        commits={contributions}
                        githubId={htmlUrl}
                        avatarUrl={avatarUrl}
                        key={login}
                    />
                )
            )}
        </Container>
    );
};

export default Contributors;
