import React from "react";
import styled from "styled-components";

const CardStyle = styled.a`
    text-decoration: none;
    position: relative;
    top: 0;
    left: 0;
    backface-visibility: hidden;
    cursor: pointer;
    width: 17%;
    margin: 10px;
    backface-visibility: hidden;
    border-radius: 5px;
    border: 1px solid currentColor;
    @media (max-width: 50em) {
        width: 80%;
    }
`;

const Tree = styled.div`
    position: relative;
    height: 150px;
    width: 100%;
    background: url("https://images.unsplash.com/photo-1511207538754-e8555f2bc187?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=88672068827eaeeab540f584b883cc66&auto=format&fit=crop&w=1164&q=80")
        no-repeat;
    background-size: cover;
    border-top-right-radius: 5px;
    border-top-left-radius: 5px;
`;

const Avatar = styled.div`
    position: relative;
    top: -60px;
    height: 120px;
    width: 120px;
    margin: 0 auto;
    border-radius: 50%;
    border: 5px solid currentColor;
    background: currentColor;
    background-size: contain;
    backface-visibility: hidden;
    overflow: hidden;
    z-index: 10;
`;

const Txt = styled.div`
    position: relative;
    top: -55px;
    margin: 0 auto;
    text-align: center;
    font-family: "Montserrat";
    font-size: 18px;
    backface-visibility: hidden;
`;

const NameStyle = styled.div`
    font-weight: 700;
    font-family: "Oswald";
    font-size: 20px;
`;

const Commits = styled.p`
    position: relative;
    top: -5px;
    font-size: 14px;
    letter-spacing: 0.4px;
    font-weight: 400;
    font-family: "Montserrat", sans-serif;
`;

const GitLink = styled.span`
    position: relative;
    top: 10px;
    font-size: 10px;
    font-weight: 700;
    letter-spacing: 0.4px;
    padding: 8px 15px;
    word-break: break-all;
`;
const Image = styled.img`
    width: 100%;
    height: 100%;
`;

const Card = ({ name, commits, githubId, avatarUrl }) => {
    return (
        <CardStyle href={githubId} rel="noopener noreferrer" target="_blank">
            <Tree></Tree>
            <Avatar>
                <Image src={avatarUrl} alt={name} />
            </Avatar>
            <Txt>
                <NameStyle>{name}</NameStyle>
                <Commits>
                    <i className="fas fa-map-marker-alt front-icons"></i>Commits
                    :- {commits}
                </Commits>
                <GitLink>{githubId}</GitLink>
            </Txt>
        </CardStyle>
    );
};

export default Card;
