import styled from "styled-components";
import React from "react";

const Header = styled.div`
    width: 100vw;
    padding: 2% 2% 2% 2%;
    font-size: 1.7rem;
`;

const ContributorsHeader = () => {
    return (
        <Header>
            <h1>NEST Contributors</h1> <br />
        </Header>
    );
};

export default ContributorsHeader;
