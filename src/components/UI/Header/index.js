import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
    MenuLink,
    Navbar,
    Menu,
    Logo,
    Hamburger,
    StyledSun as Sun,
    StyledMoon as Moon,
} from "./UI";
import ExternalLink from "../Utility/ExternalLink";

let MenuLinks = [
    { name: "Home", link: "/" },
    { name: "Announcements", link: "/announcements" },
    { name: "Contributors", link: "/contributors" },
    { name: "Documentation", link: "https://www.google.com", isExternal: true },
    {
        name: "Mailing list",
        link: "https://groups.google.com/g/nest-users",
        isExternal: true,
    },
    { name: "Events", link: "/events" },
    { name: "Getting Started", link: "/gettingStarted" },
];

const setActive = (activeIndex) => {
    MenuLinks = MenuLinks.map((item) => {
        item["active"] = false;
        return item;
    });
    MenuLinks[activeIndex].active = true;
};

const Header = ({ themeHandler, theme, active }) => {
    const [isOpen, setIsOpen] = useState(false);
    let activeIndex = MenuLinks.findIndex(({ link }) => link === active);
    const notFound = -1;
    const base = 0;
    if (activeIndex === notFound) activeIndex = base;
    setActive(activeIndex);
    return (
        <Navbar>
            <Logo>
                <Link to="/">NeST</Link>
            </Logo>
            <Hamburger onClick={() => setIsOpen(!isOpen)}>
                <span />
                <span />
                <span />
            </Hamburger>
            <Menu isOpen={isOpen}>
                <MenuLink key="theme">
                    {theme === "dark" ? (
                        <Sun onClick={themeHandler} />
                    ) : (
                        <Moon onClick={themeHandler} />
                    )}
                </MenuLink>
                {MenuLinks.map(({ name, link, isExternal, active }) => {
                    if (!isExternal)
                        return (
                            <MenuLink key={name} active={active}>
                                <Link to={link}>{name}</Link>
                            </MenuLink>
                        );
                    return (
                        <MenuLink key={name}>
                            <ExternalLink link={link}>{name}</ExternalLink>
                        </MenuLink>
                    );
                })}
            </Menu>
        </Navbar>
    );
};

export default Header;
