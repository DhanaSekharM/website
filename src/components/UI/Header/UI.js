import styled, { css } from "styled-components";
import { Sun } from "@styled-icons/boxicons-regular/Sun";
import { Moon } from "@styled-icons/boxicons-regular/Moon";
import theme from "styled-theming";
import {
    headerHoverDark,
    headerHoverLight,
    headerActiveDark,
    headerActiveLight,
} from "../../../assets/theme";
import { headerFooterTheme as headerTheme } from "../../../assets/theme";

const hoverTheme = theme("mode", {
    light: css`
        color: ${headerHoverLight};
    `,
    dark: css`
        color: ${headerHoverDark};
    `,
});

const activeTheme = theme("mode", {
    light: css`
        color: ${headerActiveLight};
    `,
    dark: css`
        color: ${headerActiveDark};
    `,
});

export const StyledSun = styled(Sun)`
    width: 2em;
    height: 2em;
    ${headerTheme};
    > * {
        ${headerTheme};
        &:hover {
            ${hoverTheme};
        }
    }
`;
export const StyledMoon = styled(Moon)`
    width: 2em;
    height: 2em;
    ${headerTheme};
    > * {
        ${headerTheme};
    }
    &:hover > * {
        ${hoverTheme};
    }
`;

export const MenuLink = styled.div`
    padding: 1rem 2rem;
    cursor: pointer;
    text-align: center;
    font-size: 1rem;
    ${headerTheme};
    @media (max-width: 50em) {
        width: 100vw;
        box-shadow: 0 5px 20px 0 rgba(0, 0, 0, 0.2);
    }
    a {
        text-decoration: none;
        ${headerTheme};
        &:hover {
            ${hoverTheme};
            ${(props) => (props.active ? activeTheme : null)};
        }
        ${(props) => (props.active ? activeTheme : null)};
    }
`;

export const Navbar = styled.div`
    padding: 0 2rem;
    display: flex;
    justify-content: space-between;
    position: sticky;
    z-index: 1000;
    align-items: center;
    flex-wrap: wrap;
    ${headerTheme};
    top: 0;
    left: 0;
    right: 0;
`;

export const Logo = styled.div`
    padding: 0.5rem;
    cursor: pointer;
    font-weight: 800;
    font-size: 1.7rem;
    ${headerTheme};
    span {
        font-weight: 300;
        font-size: 1.5rem;
        ${headerTheme};
    }
    a {
        text-decoration: none;
        ${headerTheme};
    }
    margin: 0.5em 0;
`;

export const Menu = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
    ${headerTheme};
    @media (max-width: 50em) {
        overflow: hidden;
        flex-direction: column;
        transition: max-height 0.3s ease-in;
        width: 100vw;
        max-height: ${({ isOpen }) => (isOpen ? "40em" : "0")};
        padding-bottom: ${({ isOpen }) => (isOpen ? "0.5em" : "0")};
    }
`;

export const Hamburger = styled.div`
    display: none;
    cursor: pointer;
    ${headerTheme};
    background: currentColor;
    span {
        height: 0.01em;
        width: 1.5em;
        margin-bottom: 0.25em;
        border-radius: 0.25em;
    }
    @media (max-width: 50em) {
        display: flex;
        flex-direction: column;
    }
`;
