import React from "react";
import styled from "styled-components";

const UnStyledLink = styled.a`
    text-decoration: none;
`;

const ExternalLink = ({ link, children }) => {
    return (
        <UnStyledLink href={link} rel="noopener noreferrer" target="_blank">
            {children}
        </UnStyledLink>
    );
};
export default ExternalLink;
