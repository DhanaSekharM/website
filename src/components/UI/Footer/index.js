import React from "react";
import { FlexContainer, FooterContainer } from "./UI";
import ContactUs from "./ContactUs";
import People from "./People";
import Resources from "./Resources";
import Copyright from "./Copyright";
const Footer = () => {
    return (
        <FlexContainer isColumn noBottom>
            <FooterContainer>
                <ContactUs />
                <People />
                <Resources />
            </FooterContainer>
            <Copyright />
        </FlexContainer>
    );
};
export default Footer;
