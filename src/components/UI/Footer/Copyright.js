import React from "react";
import { H4 } from "./UI";
const Copyright = () => {
    return (
        <H4>
            Copyright © 2019-21 Wireless Information Networking Group (WiNG),
            NITK Surathkal, India. All Rights Reserved.
        </H4>
    );
};
export default Copyright;
