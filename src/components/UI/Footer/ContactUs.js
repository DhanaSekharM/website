import React, { useState } from "react";
import { FlexContainer, FooterHeading, MailToSection, SecondLabel } from "./UI";
import ExternalLink from "../Utility/ExternalLink";
const ContactUs = () => {
    const [name, setName] = useState();
    const [subject, setSubject] = useState();
    const [body, setBody] = useState();
    const mailAddress = "nest@nitk.edu.in";

    return (
        <MailToSection>
            <FooterHeading>Contact Us</FooterHeading>
            <FlexContainer>
                <label htmlFor="name">Name</label>
                <input
                    type="text"
                    onChange={(e) => setName(e.target.value)}
                    id="name"
                />
                <SecondLabel htmlFor="subject">Subject</SecondLabel>
                <input
                    type="text"
                    onChange={(e) => setSubject(e.target.value)}
                    id="subject"
                />
            </FlexContainer>
            <label htmlFor="mailBody">
                Body
                <br />
            </label>
            <textarea
                onChange={(e) =>
                    setBody(e.target.value.replace(/\n/g, "%0D%0A"))
                }
                id="mailBody"
            />
            <ExternalLink
                link={`mailto:${mailAddress}?subject=${subject}&body=${body}%0D%0AWith Regards,%0D%0A${name}`}
            >
                Send
            </ExternalLink>
        </MailToSection>
    );
};

export default ContactUs;
