import React from "react";
import { FlexContainer, FooterHeading } from "./UI";
import ExternalLink from "../Utility/ExternalLink";
const ResourceList = [
    {
        name: "NeST Paper",
        link: "https://irtf.org/anrw/2020/anrw2020-final20.pdf",
    },
    { name: "Documentation", link: "https://abcd" },
    {
        name: "Users Mailing List",
        link: "https://groups.google.com/g/nest-users",
    },
    { name: "Dev Mailing List", link: "https://groups.google.com/g/nest-dev" },
    { name: "Open Issues", link: "https://gitlab.com/nitk-nest/nest/-/issues" },
];
const Resources = () => {
    return (
        <FlexContainer isColumn>
            <FooterHeading>Resources</FooterHeading>
            {ResourceList.map(({ name, link }) => (
                <ExternalLink key={name} link={link}>
                    {name}
                </ExternalLink>
            ))}
        </FlexContainer>
    );
};
export default Resources;
