import React from "react";
import Announcement from "./Announcement";

import styled from "styled-components";
import { announcementData as data } from "../../assets/constants";

const Announcements = () => {
    return (
        <AnnouncementsContainer>
            <h1>Announcements</h1>
            {data.map((d, key) => {
                return (
                    <Announcement
                        key={key}
                        heading={d.announcementHeading}
                        description={d.announcementDesc}
                    />
                );
            })}
        </AnnouncementsContainer>
    );
};

export default Announcements;

const AnnouncementsContainer = styled.div`
    align-items:center ::-webkit-scrollbar {
        display: none;
    }

    > h1 {
        text-align: center;
        font-size: 40px;
        font-weight: 600;
    }
`;
