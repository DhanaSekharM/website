import React from "react";
import styled from "styled-components";
const AnnouncementContainer = styled.div`
    padding: 10px;
    border: 1px solid currentColor;
    margin-bottom: 20px !important;
    border-radius: 10px;
    transition: transform 100ms ease-in;
    max-width: 1100px;
    margin: auto;
    @media (max-width: 50em) {
        margin-inline: 3em;
    }
    > p > a {
        text-decoration: none;
        font-size: 15px;
    }

    :hover {
        transform: scale(1.07);
    }
`;

const Announcement = ({ heading, description }) => {
    return (
        <AnnouncementContainer>
            <h2>{heading}</h2>
            <p>{description}</p>
        </AnnouncementContainer>
    );
};

export default Announcement;
