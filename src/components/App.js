import React, { useState } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import Home from "./Home";
import Contributors from "./Contributors";
import GettingStarted from "./GettingStarted";
import Events from "./Events";
import Announcements from "./Announcements";
import Header from "./UI/Header";
import Footer from "./UI/Footer";
import ScrollToTop from "./Utility/ScrollToTop";
import { GlobalStyles } from "../assets/theme";
const App = () => {
    const [theme, setTheme] = useState("dark");
    const [active, setActive] = useState("/");
    return (
        <ThemeProvider theme={{ mode: theme }}>
            <HashRouter>
                <GlobalStyles />
                <ScrollToTop activator={(val) => setActive(val)} />
                <Header
                    themeHandler={() =>
                        setTheme((theme) =>
                            theme === "dark" ? "light" : "dark"
                        )
                    }
                    theme={theme}
                    active={active}
                />
                <Switch>
                    <Route
                        path="/contributors"
                        exact
                        component={Contributors}
                    />
                    <Route
                        path="/gettingStarted"
                        exact
                        component={GettingStarted}
                    />
                    <Route path="/events" exact component={Events} />
                    <Route
                        path="/announcements"
                        exact
                        component={Announcements}
                    />
                    <Route path="/" component={Home} />
                </Switch>
                <Footer />
            </HashRouter>
        </ThemeProvider>
    );
};
export default App;
