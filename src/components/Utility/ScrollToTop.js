import { useEffect } from "react";
import { useLocation } from "react-router-dom";

const zero = 0;

const ScrollToTop = ({ activator }) => {
    const { pathname } = useLocation();
    useEffect(() => {
        window.scrollTo(zero, zero);
        activator(pathname);
    }, [pathname]);
    return null;
};
export default ScrollToTop;
