import { createGlobalStyle, css } from "styled-components";
import theme from "styled-theming";

export const primaryLight = "#ffffff";
export const secondaryLight = "#000c33";
export const primaryDark = "#00081d";
export const secondaryDark = "#d49100";
export const headerHoverDark = "#FF0000";
export const headerHoverLight = "#00b418";
export const headerActiveDark = "#FF0000";
export const headerActiveLight = "#00b418";

const colorTheme = theme("mode", {
    light: css`
        background-color: ${primaryLight};
        color: ${secondaryLight};
    `,
    dark: css`
        background-color: ${primaryDark};
        color: ${secondaryDark};
    `,
});

export const reverseTheme = theme("mode", {
    light: css`
        background-color: ${secondaryLight};
        color: ${primaryLight};
    `,
    dark: css`
        background-color: ${secondaryDark};
        color: ${primaryDark};
    `,
});

export const headerFooterTheme = theme("mode", {
    light: css`
        background-color: ${secondaryLight};
        color: ${primaryLight};
    `,
    dark: css`
        background-color: ${secondaryDark};
        color: ${primaryDark};
    `,
});

export const GlobalStyles = createGlobalStyle`
    body{
        margin: 0;
        padding: 0;
        max-width: 100vw;
    }
    #root{
        min-height: 100vh;
    }
    *{
        ${colorTheme};
    }
`;
