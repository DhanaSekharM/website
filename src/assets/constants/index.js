import Announcement from "./Announcements";
import event from "./Events";
import home from "./Home";
import gettingStarted from "./GettingStarted";
export const announcementData = Announcement;
export const eventData = event;
export const homeData = home;
export const gettingStartedData = gettingStarted;
