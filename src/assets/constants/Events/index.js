import React from "react";
export default [
    {
        name: "Conference at Institute of Technology",
        desc: (
            <>
                Lorem ipsum dolor sit amet, consecteturLorem ipsum dolor sit
                amet, consecteturLorem ipsum dolor sit amet, consecteturLorem
                ipsum dolor sit amet, consectetur
            </>
        ),
        date: "2021-03-12",
        link: "https://www.google.com",
    },
    {
        name: "Conference at Institute of Technology",
        desc: (
            <>
                Lorem ipsum dolor sit amet, consecteturLorem ipsum dolor sit
                amet, consecteturLorem ipsum dolor sit amet, consecteturLorem
                ipsum dolor sit amet, consectetur
            </>
        ),
        date: "2021-03-12",
        link: "https://www.google.com",
    },
    {
        name: "ACM 17th Wimbledon Conference",
        desc: (
            <>
                Lorem ipsum dolor sit amet, consecteturLorem ipsum dolor sit
                amet, consecteturLorem ipsum dolor sit amet, consecteturLorem
                ipsum dolor sit amet, consectetur
            </>
        ),
        date: "2021-04-13",
        link: "https://www.google.com",
    },
];
