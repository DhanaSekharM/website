export default {
    Title: "NeST",
    logo: "https://picsum.photos/100",
    descHead: "Description Heading",
    descBox: "Description content here",
    graphImg: "https://picsum.photos/100",
    codeSnippet: "python3 -m pip install nitk-nest",
    Links: "https://randomlinkhere",
};
