# This is the website repository for [NeST](https://gitlab.com/nitk-nest/nest)

NeST is a python3 package that handles testbed setup, testbed configuration,
collecting and visualizing data by providing a user-friendly API, addressing
common issues involved in conducting networking experiments.

## Website running instructions

Ensure you have both npm(>6, preferably >6.14) and node(>12, preferably>14) installed.

1. Clone the repository
2. Run `npm install`
3. Run `npx husky install`
4. Run `npm start`

## Contributing instructions

These can be found in the [Contributing.md](Contributing.md)
